#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script iterates the installer over all domain configuration files.
# It is meant to be executed _without_ root rights.

import awio
import glob
import os
from pathlib import Path
import subprocess

domain_files = glob.glob(os.path.expanduser("~opera/domains/*"))

awio.multidel(os.path.expanduser("~opera/toolchain/crontab.conf/*.job"))
for domain in domain_files:
    if not os.path.islink(domain):
        domain_name = Path(domain).stem
        subprocess.call(["python3", os.path.expanduser("~opera/toolchain/install_domain.py"), domain_name])
    else:
        print(f"[i] Skipping symlink for '{domain}'")

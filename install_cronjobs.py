#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import glob
import os

def install_jobs():
    """Install pre-generated cronjob files.

    This script must be run as root."""
    curr_jobs = glob.glob("/etc/cron.d/aws_*")
    for job in curr_jobs:
        os.remove(job) # clear all current awsome cronjobs

    templates = glob.glob(os.path.expanduser("~opera/toolchain/crontab.conf/*.job"))
    msg = [os.path.basename(templ) for templ in templates]
    print("[i] Installing cronjobs " + ", ".join(msg))

    for file in templates:
        with open(file, "r") as jobtemplate:
            data = jobtemplate.read()
            module = os.path.basename(file)[:-4]
            # crontabs must be written as root for matching ownership
            with open(f"/etc/cron.d/aws_{module}", "w") as cronjob:
                cronjob.write(f"# This file was created automatically by the awsome toolchain.\n")
                cronjob.write(data)
        os.remove(file) # has been copied out to /etc/cron.d

if __name__ == "__main__":
    install_jobs()

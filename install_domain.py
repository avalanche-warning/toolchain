#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script installs a domain according to its configuration file.
# It is meant to be executed _without_ root rights, but it will prepare some
# data for a follow-up script with elevated rights (e. g. to install cronjobs).

from awset import get, is_enabled
import sys

def install_domain(domain: str):
    _install_submodules(domain)

def _install_submodules(domain: str):
    modules = ["snowpack", "maps", "solar", "wrf", "report"]
    default_run_hours = [0, 1, 0, 18, None] # times of day to start module at (if no xml setting given)
    enabled = [is_enabled(mod, domain) for mod in modules]

    for num, mod in enumerate(modules):
        if enabled[num] == True:
            _prepare_module(mod, default_run_hours[num], domain)

def _prepare_module(module: str, run_hour: int, domain: str):
    run_on_days = get([module, "run", "days"], domain, default="*")
    run_at_hour = get([module, "run", "hour"], domain, default=str(run_hour))
    _prepare_job(module, run_on_days, run_at_hour, domain)

def _prepare_job(module: str, run_on_days: str, run_at_hour: str, domain: str):
    """This function builds one cron file per module which contains all domains.

    The resulting cron file for snowpack runs could look similar to this:
    # This file was created automatically by the awsome toolchain.
    0 5 * * * snowpack cd opera && ./run_snowpack.py nordkette >> ~opera/logs/snowpack.log 2>&1
    0 6 * * * snowpack cd opera && ./run_snowpack.py fieberbrunn >> ~opera/logs/snowpack.log 2>&1
    """
    data = ""
    with open(os.path.expanduser(f"~opera/toolchain/crontab.conf/aws_{module}.template"), "r") as jobtemplate:
        for line in jobtemplate.readlines():
            # strip comments of the very descriptive template files (which the user will edit):
            line = line.partition("#")[0].strip()
            if line:
                data = data + line + "\n"
    data = data.replace("$DOMAIN", domain)
    data = data.replace("$DAYS", run_on_days)
    data = data.replace("$HOUR", run_at_hour)

    with open(os.path.expanduser(f"~opera/toolchain/crontab.conf/{module}.job"), "a+") as cronjob:
        cronjob.write(data) # append (or start new file with) the domain's cronjob

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("[E] Synopsis: python3 install_domain.py <domain>")
    domain = sys.argv[1]
    print(f"Installing domain '{domain}'...")
    install_domain(domain)

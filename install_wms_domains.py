#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This script is part of the installation of our domains and handles the
# web map service part.
# It must be ran with root rights in order to configure our WMS software.

from awset import get

import glob
import os
from pathlib import Path
import rasterio
import shutil

_wms_path = "/opt/mapserver/share"
_templates_dir = os.path.expanduser("~opera/wms")
_mapserver_config = "/opt/mapserver/mapserver.conf"

def _map_substitutions(data: str, map_name: str, domain: str):
    demfile = get(["domain", "dem", "file"], domain)
    hostname = get(["host", "domain"], "awsome", "~opera/project") # domain of awsome server
    dem = rasterio.open(demfile)
    bb = dem.bounds
    dem_extent = f"{bb.left} {bb.bottom} {bb.right} {bb.top}"

    crs = dem.crs.items()
    dem_epsg = None
    if "init" in [item[0] for item in crs]:
        dem_epsg = crs.mapping["init"]
    elif ("proj", "lcc") in crs and ("lat_0", 47.5) in crs and ("lat_1", 46) in crs:
        dem_epsg = "epsg:31287" # pretty sure it's MGI Austrian Lambert

    if not dem_epsg:
        # Serve crs from input data and advertise the following generic codes:
        #   EPSG:3857 (web mercator)
        #   EPSG:4269, EPSG:4326 (lat/lon)
        # This should present data, but clients may be confused if the meta data does not match.
        data = data.replace("$PROJECTION", "AUTO")
        data = data.replace("$ADVERTISED_PROJECTION", "EPSG:3857 EPSG:4269 EPSG:4326")
    else:
        data = data.replace("$PROJECTION", '"init=' + dem_epsg + '"')
        data = data.replace("$ADVERTISED_PROJECTION", dem_epsg.upper())

    data = data.replace("$EXTENT", dem_extent)
    data = data.replace("$MAXSIZE", str(max(dem.shape)))
    data = data.replace("$WIDTH", str(dem.shape[1]))
    data = data.replace("$HEIGHT", str(dem.shape[0]))
    data = data.replace("$DEMPATH", os.path.dirname(demfile))
    data = data.replace("$DEMDATA", os.path.basename(demfile))
    data = data.replace("$HOST", f"wms.{hostname}")
    data = data.replace("$NAME", map_name)
    data = data.replace("$DOMAIN", domain)
    return data

def install_wms(domain: str):
    wms_path = f"{_wms_path}/maps/{domain}"
    os.makedirs(wms_path, exist_ok=True)
    data_path = f"{_wms_path}/data/{domain}"
    os.makedirs(data_path, exist_ok=True)
    shutil.chown(data_path, group="opera")
    os.chmod(data_path, 0o775) # write access for group

    mapfiles = glob.glob(f"{_templates_dir}/maps/*.map")
    for fmap in mapfiles:
        map_name = Path(os.path.basename(fmap)).stem
        with open(fmap, "r") as mapfile:
            data = mapfile.read()
            data = _map_substitutions(data, map_name, domain)
        with open(f"{_wms_path}/maps/{domain}/{map_name}.map", "w") as outfile:
            outfile.write(f"# This config file was auto-created by '{__file__}'.\n")
            outfile.write(data)
        with open(_mapserver_config, "r") as conf:
            data = conf.read()
            data = data.replace("#$MAP_NAME $MAP_PATH", f'{map_name}_{domain} "{_wms_path}/maps/{domain}/{map_name}.map"'
                + "\n    #$MAP_NAME $MAP_PATH") # repeat the pattern for next domain
        with open(_mapserver_config, "w") as conf:
            conf.write(data)

if __name__ == "__main__":
    # The config file is replaced with the template before iterating over the domains to start fresh.
    # A single domain will then install itself alongside any others (a list of mapfiles is built):
    _mapserver_config = "/opt/mapserver/mapserver.conf"
    try:
        os.remove(_mapserver_config)
    except FileNotFoundError:
        pass
    shutil.copyfile(f"{_templates_dir}/mapserver.conf", _mapserver_config)

    domain_files = glob.glob(os.path.expanduser("~opera/domains/*"))
    for domain in domain_files:
        if not os.path.islink(domain):
            domain_name = Path(domain).stem
            install_wms(domain_name)

#!/usr/bin/env python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Script to prepare various data on domain installation.

from awgeo import raster2asc
from awset import get
import glob
import os
from pathlib import Path
import sys

def prepare_data():
    domain_files = glob.glob(os.path.expanduser("~opera/domains/*"))
    for fdom in domain_files:
        if not os.path.islink(fdom):
            domain = Path(fdom).stem
            dem_file = get(["domain", "dem", "file"], domain)
            _, ext = os.path.splitext(dem_file)
            if (ext in [".tif", ".tiff", ".gtif", ".gtiff"]):
                print(f"[i] Converting DEM for domain '{domain}'")
                raster2asc(dem_file)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        sys.exit("[E] Synopsis: python3 prepare_data.py")
    prepare_data()

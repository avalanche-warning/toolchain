import os
import paramiko
import subprocess

def rsa_keygen(key_file: str, bits=2048, password=None):
    key = paramiko.RSAKey.generate(bits)
    key.write_private_key_file(key_file, password)
    with open(key_file + ".pub", "w") as outfile:
        outfile.write(key.get_base64())

def copy_to_host(user: str, host: str, key_file=None):
    if not key_file:
        key_file = get_default_keyloc()
    subprocess.call(["ssh-copy-id", "-i", key_file, f"{user}@{host}"])

def get_default_keyloc() -> str:
    return os.path.expanduser("~/.ssh/id_rsa")

if __name__ == "__main__":
    rsa_keygen(get_default_keyloc())
